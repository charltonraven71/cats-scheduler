﻿using Quartz;
using System;
using System.Threading.Tasks;


class BusinessJob : IJob
{
    public async Task Execute(IJobExecutionContext context)
    {
        String Qual = "B";
        RunUtil run = new RunUtil(Qual, DateTime.Now);
        run.GetAndExecute();
    }
}
