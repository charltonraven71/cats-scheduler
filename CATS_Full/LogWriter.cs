﻿using System;
using System.IO;


class LogWriter
{
    public String filename;
    public String message;
    public StreamWriter swFile = null;

    public LogWriter()
    {

    }
    public LogWriter(string filename)
    {
        this.filename = filename;
    }

    public LogWriter(string filename, string message)
    {
        this.filename = filename;
        this.message = message;
    }

    public void write(String message)
    {
        swFile = new StreamWriter(filename, true);
        swFile.Write(message);
        close();
    }
    public void writeLine(String message)
    {
        try
        {
            swFile = new StreamWriter(filename, true);
            swFile.WriteLine(message);
            close();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }

    }


    public void close()
    {
        swFile.Close();
        swFile.Dispose();

    }
    public bool deleteLog()
    {
        FileInfo file = new FileInfo(filename);
        file.Delete();

        return file.Exists;
    }
}

