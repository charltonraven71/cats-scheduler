﻿using CATS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Linq;


class RunUtil
{
    public String logFilename = "";
    public String Qual;
    public DateTime currentDate;
    private List<String> logQueue = new List<String>();
    public LogWriter log = null;
    public static String CurrentYear = DateTime.Now.Year.ToString();
    public static String CurrentDayOfWeek = DateTime.Now.DayOfWeek.ToString();
    public static String currentDayOfWeekNum = DayOfWeek2Number(CurrentDayOfWeek.ToUpper());
    public static String CurrentDayOfMonth = DateTime.Now.Day.ToString();
    public static String DOY = DateTime.Now.DayOfYear.ToString();
    public static String CurrentDate = DateTime.Now.ToString("yyyyMMdd");
    public static String CurrentTime = DateTime.Now.ToString("HHmm");
    public static String CurrentHour = DateTime.Now.ToString("HH");
    public static String CurrentMinute = DateTime.Now.ToString("mm");
    public static String [] BusinessDays = { "2", "3", "4", "5", "6" };



    public RunUtil(String Qual, DateTime currentDate)
    {
        this.Qual = Qual;
        this.currentDate = currentDate;

        String logFileDateStr = currentDate.ToString("yyyyMMddHHmmss");
        logFilename = JobScheduler.LogDirectory + "CATS_" + Qual + "_" + logFileDateStr + ".dat";
    }


    public void GetAndExecute()
    {
        String currentDateSTR = currentDate.ToString("MM/dd/yyyy HH:mm:ss");
        log = new LogWriter(logFilename);

        //Get Schedules for the time slot
        List<RunInfo> runScripts = new List<RunInfo>();
        DateTime date = DateTime.Now;
        String CurrentDate = DateTime.Now.ToString("yyyyMMdd");
        String CurrentTime = DateTime.Now.ToString("HHmm");

        JobScheduler.schs.ForEach(sch =>
        {

            if (Qual.Equals("B") && sch.TimeQual.Equals(Qual))
            {
                if (sch.Time.Equals(CurrentTime) && BusinessDays.Contains(currentDayOfWeekNum))
                {
                    runScripts.Add(new RunInfo(sch.Time, sch.Script, sch.Description));
                }
            }
            if (Qual.Equals("D") && sch.TimeQual.Equals(Qual))
            {
                if (sch.Time.Equals(CurrentTime))
                {
                    runScripts.Add(new RunInfo(sch.Time, sch.Script, sch.Description));
                }
            }
            if (Qual.Equals("M") && sch.TimeQual.Equals(Qual))
            {
                if (sch.Day.Equals(CurrentDayOfMonth) && sch.Time.Equals(CurrentTime))
                {
                    runScripts.Add(new RunInfo(sch.Time, sch.Script, sch.Description));
                }
            }
            if (Qual.Equals("W") && sch.TimeQual.Equals(Qual))
            {
                if (sch.Day.Equals(currentDayOfWeekNum) && sch.Time.Equals(CurrentTime))
                {
                    runScripts.Add(new RunInfo(sch.Time, sch.Script, sch.Description));
                }
            }
            if (Qual.Equals("Y") && sch.TimeQual.Equals(Qual))
            {
                if (sch.Day.Equals(DOY) && sch.Time.Equals(CurrentTime))
                {
                    runScripts.Add(new RunInfo(sch.Time, sch.Script, sch.Description));
                }
            }
            if (Qual.Equals("0") && sch.TimeQual.Equals(Qual))
            {
                if (CurrentTime.EndsWith("0"))
                {
                    runScripts.Add(new RunInfo(sch.Time, sch.Script, sch.Description));
                }
            }
            if (Qual.Equals("5") && sch.TimeQual.Equals(Qual))
            {
                if (CurrentTime.EndsWith("5"))
                {
                    runScripts.Add(new RunInfo(sch.Time, sch.Script, sch.Description));
                }
            }
            if (Qual.Equals("C") && sch.TimeQual.Equals(Qual))
            {
                FileInfo CalendarFile = new FileInfo(JobScheduler.CalendarDirectory + "SON_CATSCal_" + sch.CalendarName + ".dat");
                if (CalendarFile.Exists)
                {
                    StreamReader sr = new StreamReader(CalendarFile.FullName);
                    String line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (!line.StartsWith("#") || !line.Trim().Equals(""))
                        {
                            String sYear = line.Split('|')[0];
                            String sDOY = line.Split('|')[1];
                            if (sYear.Equals(CurrentYear) && sDOY.Equals(DOY) && sch.Time.Equals(CurrentTime))
                            {
                                runScripts.Add(new RunInfo(sch.Time, sch.Script, sch.Description));

                            }
                        }

                    }
                    sr.Close();
                }
                else
                {
                    log.writeLine("Missing " + CalendarFile.Name + ". Please add Calendar file to " + JobScheduler.CalendarDirectory);
                    Email ErrorEmail = new Email(new FileInfo(JobScheduler.MainEmail), "CATS Notification ERROR: Missing Calendar", "Missing " + CalendarFile.Name + ". Please add Calendar file to " + JobScheduler.CalendarDirectory);
                    ErrorEmail.Send();
                }

            }
        });





        //Run Schedules in time slot
        if (runScripts.Count != 0)
        {

            Console.WriteLine(String.Format("{0} Job at {1}", Qual, currentDateSTR) + "\n" + "Running " + runScripts.Count + " Job(s)");
            log.writeLine(String.Format("{0} Job at {1}", Qual, currentDateSTR) + "\n" + "Running " + runScripts.Count + " Job(s)");



            List<Task> tasks = new List<Task>();
            for (int i = 0; i < runScripts.Count; i++)
            {
                RunInfo run = runScripts[i];
                if (FileExist(run.Script))
                {
                    Task task = Task.Factory.StartNew(() => logQueue.AddRange(runJobs(run.Script)));
                    tasks.Add(task);
                }
                else
                {
                    log.writeLine("Missing " + run.Script + ". Please add script to " + JobScheduler.ScriptsDirectory);
                    Email ErrorEmail = new Email(new FileInfo(JobScheduler.MainEmail), "CATS Notification ERROR: Missing Script", "Missing " + run.Script + ". Please add script to " + JobScheduler.ScriptsDirectory);
                    ErrorEmail.Send();
                }
            }
            Task t = Task.WhenAll(tasks.ToArray());
            try
            {

                //Wait for all task to complete for the timeslot and dispose of all the tasks
                t.Wait();
                tasks.ForEach(task => task.Dispose());
                t.Dispose();

                //Write Log Messages in Queue
                logQueue.ForEach(message => log.writeLine(message));
                logQueue.Clear();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    private static List<String> runJobs(String Script)
    {
        List<String> logQueue = new List<string>();

        Console.WriteLine("Running  " + Script);
        logQueue.Add("Running  " + Script);

        ProcessStartInfo startInfo = new ProcessStartInfo();
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        startInfo.FileName = "cmd.exe";
        startInfo.UseShellExecute = false;

        startInfo.WorkingDirectory = JobScheduler.ScriptsDirectory;


        if (Script.EndsWith(".pl"))
        {
            startInfo.FileName = "perl.exe";
            startInfo.Arguments = JobScheduler.ScriptsDirectory + Script;
        }
        if (Script.EndsWith(".exe"))
        {
            startInfo.Arguments = "/C " + Script;
        }
        if (Script.EndsWith(".bat"))
        {
            startInfo.Arguments = "/C " + Script;
        }
        if (Script.EndsWith(".class"))
        {
            startInfo.Arguments = "/C java " + Script.Replace(".class", "");
        }


        Process proc = new Process();
        proc.StartInfo = startInfo;
        proc.StartInfo.RedirectStandardError = true;
        proc.StartInfo.UseShellExecute = false;
        proc.Start();
        //Get Error output of the ran program
        String ErrorOut = proc.StandardError.ReadToEnd();
        if (!ErrorOut.Equals("") && JobScheduler.EmailOn)
        {
            Email ErrorEmail = new Email(new FileInfo(JobScheduler.MainEmail), "CATS Notification ERROR: Script Error: " + Script, ErrorOut + "\n\nPlease fix the issue and rerun.");
            ErrorEmail.Send();
        }
        proc.WaitForExit(1500000);
        Console.WriteLine("Finished  " + Script);
        logQueue.Add("Finished " + Script);

        return logQueue;
    }

    private static bool FileExist(String script)
    {
        FileInfo FileScript = new FileInfo(JobScheduler.ScriptsDirectory + script);
        bool exist = FileScript.Exists;
        return exist;
    }

    private static String DayOfWeek2Number(String dayOfWeek)
    {
        String dayNum = "";
        if (dayOfWeek.Equals("SUNDAY"))
        {
            dayNum = "1";
        }
        else if (dayOfWeek.Equals("MONDAY"))
        {
            dayNum = "2";
        }
        else if (dayOfWeek.Equals("TUESDAY"))
        {
            dayNum = "3";
        }
        else if (dayOfWeek.Equals("WEDNESDAY"))
        {
            dayNum = "4";
        }
        else if (dayOfWeek.Equals("THURSDAY"))
        {
            dayNum = "5";
        }
        else if (dayOfWeek.Equals("FRIDAY"))
        {
            dayNum = "6";
        }
        else if (dayOfWeek.Equals("SATURDAY"))
        {
            dayNum = "7";
        }

        return dayNum;
    }
}

