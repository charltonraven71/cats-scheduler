﻿using System;
using System.IO;
using System.Net.Mail;


public class Email
{
    public String from = "edi@sonoco.com";
    public String host = "mail.sonoco.com";
    public int port = 25;
    public String subject = "";
    public String body = "";
    public String[] to;
    public String[] cc;
    public String[] bcc;
    public String[] attach;


    public Email(FileInfo emailFileDir, String subject, String body)
    {
        this.subject = subject;
        this.body = body;


        try
        {
            FileInfo file = new FileInfo(emailFileDir.FullName);
            String [] EmailArray = File.ReadAllText(file.FullName).Split('|');

            if (!EmailArray[0].Split(',')[0].Equals(""))
            {
                this.from = EmailArray[0].Split(',')[0];
            }

            if (EmailArray[1].Split(',').Length>0)
            {
                String[] toAdd = EmailArray[1].Split(',');
                to = new string[toAdd.Length];
                for(int i = 0; i < toAdd.Length; i++)
                {
                    to[i] = toAdd[i];
                }
            }

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
    }

    public Email(FileInfo emailFileDir, String subject, FileInfo bodyFile)
    {
        this.subject = subject;
        setFileMessage(bodyFile);
    }
    public Email(String from, String to, String cc, String bcc, String subject, String body)
    {
        if (!from.Equals(""))
        {
            this.from = from;
        }

        this.to = new string[1];
        this.to[0] = to;

        if (cc != null)
        {
            this.cc = new string[1];
            this.cc[0] = cc;
        }


        this.subject = subject;
        this.body = body;
    }

    public Email(String from, String to, String cc, String bcc, String subject, FileInfo bodyFile)
    {
        if (!from.Equals(""))
        {
            this.from = from;
        }

        this.to = new string[1];
        this.to[0] = to;

        if (cc != null)
        {
            this.cc = new string[1];
            this.cc[0] = cc;
        }

        if (bcc != null)
        {
            this.bcc = new string[1];
            this.bcc[0] = bcc;
        }

        this.subject = subject;
        setFileMessage(bodyFile);



    }

    public Email(String from, String[] to, String[] cc, String[] bcc, String subject, String body)
    {

        if (!from.Trim().Equals(""))
        {
            this.from = from;
        }

        this.to = to;
        this.cc = cc;
        this.bcc = bcc;


        this.subject = subject;
        this.body = body;
    }

    public Email(String from, String[] to, String[] cc, String[] bcc, String subject, FileInfo bodyFile)
    {
        if (!from.Trim().Equals(""))
        {
            this.from = from;
        }

        this.to = to;
        this.cc = cc;
        this.bcc = bcc;

        this.subject = subject;
        setFileMessage(bodyFile);
    }

    public void Send()
    {
        SmtpClient client = new SmtpClient();
        client.Port = port;
        client.DeliveryMethod = SmtpDeliveryMethod.Network;
        client.UseDefaultCredentials = false;
        client.EnableSsl = false;
        client.Host = host;

        MailMessage mail = new MailMessage();
        mail.From = new MailAddress(from);




        if (to != null)
        {
            foreach (String address in to)
            {
                mail.To.Add(address);
            }
        }

        if (cc != null)
        {
            foreach (String address in cc)
            {
                mail.CC.Add(address);
            }
        }

        if (bcc != null)
        {
            foreach (String address in bcc)
            {
                mail.Bcc.Add(address);
            }

        }


        mail.Subject = subject;
        mail.Body = body;

        client.Send(mail);
    }
    private void setFileMessage(FileInfo bodyFile)
    {
        try
        {
            body = File.ReadAllText(bodyFile.FullName);

        }
        catch (FileNotFoundException ex)
        {
            Console.WriteLine(ex.Message);
        }
    }
}

