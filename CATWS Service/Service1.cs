﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace CATWS_Service
{
    public partial class Service1 : ServiceBase
    {
        JobScheduler scheduler;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            scheduler = new JobScheduler("CATWS.Dat");
            scheduler.Start();
        }

        protected override void OnStop()
        {
            scheduler.Stop();
        }
    }
}
