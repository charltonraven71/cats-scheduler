﻿using CATS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Linq;


class RunUtilOnDemand
{
    public String logFilename = "";
    public String Qual;
    public DateTime currentDate;
    private List<String> logQueue = new List<String>();
    public LogWriter log = null;


    public RunUtilOnDemand(String Qual, DateTime currentDate)
    {
        this.Qual = Qual;
        this.currentDate = currentDate;

        String logFileDateStr = currentDate.ToString("yyyyMMddHHmmss");
        logFilename = JobScheduler.LogDirectory + "CATS_" + Qual + "_" + logFileDateStr + ".dat";
    }


    public void GetAndExecute()
    {
        String currentDateSTR = currentDate.ToString("MM/dd/yyyy HH:mm:ss");
        log = new LogWriter(logFilename);

        //Get Schedules for the time slot
        List<String> runScripts = new List<String>();
        DateTime date = DateTime.Now;
        String StrDate = DateTime.Now.ToString("yyyyMMdd");
        String StrTime = DateTime.Now.ToString("HHmm");

        //Read On Demand File
        StreamReader sr = new StreamReader(JobScheduler.OnDemandSchTbl);
        String line;
        while ((line = sr.ReadLine()) != null)
        {
            if (!line.StartsWith("#") || !line.Trim().Equals(""))
            {
                String script = line.Trim();
                script = script.TrimEnd(Environment.NewLine.ToArray());
                runScripts.Add(script);  
            }

        }
        sr.Close();
        //Empty OnDemand File
        try
        {
            //File.WriteAllText(JobScheduler.OnDemandSchTbl, string.Empty);
            StreamWriter strm = File.CreateText(JobScheduler.OnDemandSchTbl);
            strm.Flush();
            strm.Close();
            // FileInfo file = new FileInfo(JobScheduler.OnDemandSchTbl);

        }
        catch (Exception ex)
        {
            Console.WriteLine(ex);
        }

        //Run Schedules in time slot
        if (runScripts.Count != 0)
        {

            Console.WriteLine(String.Format("{0} Job at {1}", Qual, currentDateSTR) + "\n" + "Running " + runScripts.Count + " Job(s)");
            log.writeLine(String.Format("{0} Job at {1}", Qual, currentDateSTR) + "\n" + "Running " + runScripts.Count + " Job(s)");



            List<Task> tasks = new List<Task>();
            for (int i = 0; i < runScripts.Count; i++)
            {
                String script = runScripts[i];
                if (FileExist(script))
                {
                    Task task = Task.Factory.StartNew(() => logQueue.AddRange(runJobs(script)));
                    tasks.Add(task);
                }
                else
                {
                    log.writeLine("Missing " + script + ". Please add script to " + JobScheduler.ScriptsDirectory);
                    Email ErrorEmail = new Email(new FileInfo(JobScheduler.MainEmail), "CATS Notification ERROR: Missing Script", "Missing " + script + ". Please add script to " + JobScheduler.ScriptsDirectory);
                    ErrorEmail.Send();
                }
            }
            Task t = Task.WhenAll(tasks.ToArray());
            try
            {

                //Wait for all task to complete for the timeslot and dispose of all the tasks
                t.Wait();
                tasks.ForEach(task => task.Dispose());
                t.Dispose();

                //Write Log Messages in Queue
                logQueue.ForEach(message => log.writeLine(message));
                logQueue.Clear();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }

    private static List<String> runJobs(String Script)
    {
        List<String> logQueue = new List<string>();

        Console.WriteLine("Running  " + Script);
        logQueue.Add("Running  " + Script);

        ProcessStartInfo startInfo = new ProcessStartInfo();
        startInfo.WindowStyle = ProcessWindowStyle.Hidden;
        startInfo.FileName = "cmd.exe";
        startInfo.UseShellExecute = false;

        startInfo.WorkingDirectory = JobScheduler.ScriptsDirectory;


        if (Script.EndsWith(".pl"))
        {
            startInfo.FileName = "perl.exe";
            startInfo.Arguments = JobScheduler.ScriptsDirectory + Script;
        }
        if (Script.EndsWith(".exe"))
        {
            startInfo.Arguments = "/C " + Script;
        }
        if (Script.EndsWith(".bat"))
        {
            startInfo.Arguments = "/C " + Script;
        }
        if (Script.EndsWith(".class"))
        {
            startInfo.Arguments = "/C java " + Script.Replace(".class", "");
        }


        Process proc = new Process();
        proc.StartInfo = startInfo;
        proc.StartInfo.RedirectStandardError = true;
        proc.StartInfo.UseShellExecute = false;
        proc.Start();
        //Get Error output of the ran program
        String ErrorOut = proc.StandardError.ReadToEnd();
        if (!ErrorOut.Equals("") && JobScheduler.EmailOn)
        {
            Email ErrorEmail = new Email(new FileInfo(JobScheduler.MainEmail), "CATS Notification ERROR: Script Error: " + Script, ErrorOut + "\n\nPlease fix the issue and rerun.");
            ErrorEmail.Send();
        }
        proc.WaitForExit(1500000);
        Console.WriteLine("Finished  " + Script);
        logQueue.Add("Finished " + Script);

        return logQueue;
    }

    private static bool FileExist(String script)
    {
        FileInfo FileScript = new FileInfo(JobScheduler.ScriptsDirectory + script);
        bool exist = FileScript.Exists;
        return exist;
    }
}

