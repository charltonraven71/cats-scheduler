﻿using Quartz;
using System;
using System.Threading.Tasks;


class YearlyJob : IJob
{
    public async Task Execute(IJobExecutionContext context)
    {
        String Qual = "Y";
        RunUtil run = new RunUtil(Qual, DateTime.Now);
        run.GetAndExecute();
    }
}
