﻿using Quartz;
using System;
using System.Threading.Tasks;


class ZeroJob : IJob
{

    public async Task Execute(IJobExecutionContext context)
    {
        String Qual = "0";
        RunUtil run = new RunUtil(Qual, DateTime.Now);
        run.GetAndExecute();
    }
}

