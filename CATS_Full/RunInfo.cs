﻿using System;

namespace CATS
{
    class RunInfo
    {
        private String time;
        private String script;
        private String description;

        public string Time { get => time; set => time = value; }
        public string Script { get => script; set => script = value; }
        public string Description { get => description; set => description = value; }

        public RunInfo(String time, String script, String description)
        {
            this.time = time;
            this.script = script;
            this.description = description;
        }
        public RunInfo(String script)
        {
            this.script = script;
        }
    }
}
