﻿using Quartz;
using System;
using System.Threading.Tasks;


class OnDemandJob : IJob
{
    public async Task Execute(IJobExecutionContext context)
    {
        String Qual = "DEMAND";
        RunUtilOnDemand run = new RunUtilOnDemand(Qual,DateTime.Now);
        run.GetAndExecute();
    }
}
