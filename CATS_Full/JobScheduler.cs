﻿using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;
using System;
using System.Collections.Generic;
using System.IO;

public class JobScheduler
{
    private static IScheduler scheduler;
    //  public static List<Schedule> schs = new List<Schedule>();
    public static List<Schedule> schs = new List<Schedule>();
    public static String ScriptsDirectory = "";
    public static String LogDirectory = "";
    public static String CalendarDirectory = "";
    public static String ScheduleTbl = "";
    public static String OnDemandSchTbl = "";
    public static String MainEmail = "";
    public static bool EmailOn = false;

    public void Start()
    {

        //Get Configuration for program to run. If getConfigurations() returns anything greater than 0, ERRORS EXIST
        if (getConfigurations() == 0)
        {
            printDirectories();


            FileInfo emailFile = new FileInfo(MainEmail);
            //Send initial Email
            String nowDate = DateTime.Now.ToString("dddd, MMMM dd, yyyy hh:mm:ss tt");
            Console.WriteLine(String.Format("Start Date: {0}\n", nowDate));

            if (EmailOn)
            {
                try
                {
                    Email startEmail = new Email(emailFile, "CATS Notification: Scheduler Started at " + nowDate, "CATS Scheduler " + String.Format("Start Date: {0}\n", nowDate));
                    startEmail.Send();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            //Initialize Schedule Table
            Console.Write("Initializing Table......");
            UpdateTableJob.UpdateTable();
            Console.Write("Complete\n\n");

            //Start Quarts Scheduler Setup
            ISchedulerFactory schedulerFactory = new StdSchedulerFactory();
            scheduler = schedulerFactory.GetScheduler().GetAwaiter().GetResult();

            scheduler.Start();
            Console.WriteLine("Starting Scheduler");

            //Add Jobs to Schedule
            AddUpdateTableJob();
            AddOnDemandJob();
            AddFiveJob();
            AddZeroJob();
            AddDailyJob();
            AddBusinessJob();
            AddWeeklyJob();
            AddMonthlyJob();
            AddYearlyJob();
            AddCalendarJob();
        }
        else
        {
            Console.ReadKey();
        }

    }
    public void Stop()
    {
        //Send Ending Email
        FileInfo emailFile = new FileInfo(MainEmail);
        String endDate = DateTime.Now.ToString("dddd, MMMM dd, yyyy hh:mm:ss tt");
        Console.WriteLine(String.Format("End Date: {0}\n", endDate));

        if (EmailOn)
        {
            try
            {
                Email endEmail = new Email(emailFile, "CATS Notification: Scheduler Ending at " + endDate, "CATS Scheduler " + String.Format("End Date: {0}\n", endDate));
                endEmail.Send();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        scheduler.Shutdown();



    }
    public int getConfigurations()
    {
        String architecture = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE");

        CheckFileExists(architecture);
        //Check for Configuration File
        String configFile = "";
        if (architecture.Equals("x86"))
        {
            configFile = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\Sonoco EDI\CATS\CATS.dat";
        }
        else if (architecture.Equals("x64"))
        {
            configFile = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Sonoco EDI\CATS\CATS.dat";
        }

        Console.WriteLine(configFile);


        StreamReader sr = null;
        if (FileExist(configFile))
        {
            Console.WriteLine("Getting Configurations for Files and Directories...\n");
            sr = new StreamReader(configFile);
        }
        else
        {
            Console.WriteLine("CATS.dat file does not exist in Progrm Files Directory");
            Console.Read();
            Environment.Exit(0);
        }

        String line;
        while ((line = sr.ReadLine()) != null)
        {
            if (line.StartsWith("ScriptsDirectory"))
            {
                ScriptsDirectory = line.Split('=')[1].Replace(Environment.NewLine, "");
                if (!ScriptsDirectory.EndsWith("\\") && !ScriptsDirectory.Equals("")) { ScriptsDirectory = ScriptsDirectory + "\\"; }
            }
            if (line.StartsWith("LogDirectory"))
            {
                LogDirectory = line.Split('=')[1].Replace(Environment.NewLine, "");
                if (!LogDirectory.EndsWith("\\")&&!LogDirectory.Equals("")) { LogDirectory = LogDirectory + "\\"; }

            }
            if (line.StartsWith("CalendarDirectory"))
            {
                CalendarDirectory = line.Split('=')[1].Replace(Environment.NewLine, "");
                if (!CalendarDirectory.EndsWith("\\") && !CalendarDirectory.Equals("")) { CalendarDirectory = CalendarDirectory + "\\"; }

            }
            if (line.StartsWith("ScheduleTbl"))
            {
                ScheduleTbl = line.Split('=')[1].Replace(Environment.NewLine, "");

            }
            if (line.StartsWith("OnDemandSch"))
            {
                OnDemandSchTbl = line.Split('=')[1].Replace(Environment.NewLine, "");

            }
            if (line.StartsWith("MainEmail"))
            {
                MainEmail = line.Split('=')[1].Replace(Environment.NewLine, "");
            }
            if (line.StartsWith("EmailOn"))
            {
                String emailOn = line.Split('=')[1].Replace(Environment.NewLine, "");
                if (emailOn.ToLower().Equals("true"))
                {
                    EmailOn = true;
                }
                else if (emailOn.ToLower().Equals("false"))
                {
                    EmailOn = false;
                }
            }
        }


        int errorCount = 0;
        Dictionary<String, String> checkFiles = new Dictionary<string, string>();
        checkFiles.Add("Scripts Directory", ScriptsDirectory);
        checkFiles.Add("Log Directory", LogDirectory);
        checkFiles.Add("Calendar Directory", CalendarDirectory);
        checkFiles.Add("Schedule Table", ScheduleTbl);
        checkFiles.Add("OnDemand Schedule Table", OnDemandSchTbl);
        checkFiles.Add("Main Email", MainEmail);


        foreach (KeyValuePair<String, String> item in checkFiles)
        {
            String FileDesc = item.Key;
            String FileLocation = item.Value;
            if (FileLocation.Equals(""))
            {
                errorCount++;
                Console.WriteLine(errorCount + ". " + FileDesc +" is missing in CATS.dat file");
                
            }
            else
            {
                if (!DirectoryExist(FileLocation) && !FileExist(FileLocation))
                {
                    errorCount++;
                    Console.WriteLine(errorCount+". "+FileDesc +" does not exist on PC");
                    
                }
            }
        }
        
        if (errorCount > 0)
        {
            Console.WriteLine("\n\nThis window will close in 30 Seconds.\n\n");
            System.Threading.Thread.Sleep(30000);
        }


        return errorCount;

    }

    public static void CheckFileExists(String architecture)
    {

        String configFile = "";

        if (architecture.Equals("x86"))
        {
            configFile = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\Sonoco EDI\CATS\CATS.dat";
        }
        else if (architecture.Equals("x64"))
        {
            configFile = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Sonoco EDI\CATS\CATS.dat";
        }


        if (!FileExist(configFile))
        {
            FileInfo createCATSFile = new FileInfo(configFile);
            createCATSFile.Create();
            Console.WriteLine("CATS.dat file did not exist. A CATS.dat file has been created  in the " + createCATSFile.Directory + " folder. Application has exited. Please enter necessary information into the CATS.dat file and rerun the application.");
            Console.Read();
            Environment.Exit(0);
        }
    }

    //Production
    public static void AddUpdateTableJob()
    {
        UpdateTableJob myJob = new UpdateTableJob(); //This Constructor needs to be parameterless
        JobDetailImpl jobDetail = new JobDetailImpl("JobUpdateTbl", "GroupUpdateTbl", myJob.GetType());
        CronTriggerImpl trigger = new CronTriggerImpl("TriggerTbl", "GroupTbl", "30 * * * * ?");
        scheduler.ScheduleJob(jobDetail, trigger);
        DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();
    }
    public static void AddFiveJob()
    {
        FiveJob myJob = new FiveJob(); //This Constructor needs to be parameterless
        JobDetailImpl jobDetail = new JobDetailImpl("JobFive", "GroupFive", myJob.GetType());
        CronTriggerImpl trigger = new CronTriggerImpl("TriggerFive", "GroupFive", "0 5,15,25,35,45,55 * * * ?");
        scheduler.ScheduleJob(jobDetail, trigger);
        DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();

    }
    public static void AddCalendarJob()
    {
        CalendarJob myJob = new CalendarJob(); //This Constructor needs to be parameterless
        JobDetailImpl jobDetail = new JobDetailImpl("JobCAL", "GroupCAL", myJob.GetType());
        CronTriggerImpl trigger = new CronTriggerImpl("TriggerCAL", "GroupCAL", "0 0/5 * * * ?");
        //CronTriggerImpl trigger = new CronTriggerImpl("TriggerCAL", "GroupCAL", "0 */2 * * * ?");
        scheduler.ScheduleJob(jobDetail, trigger);
        DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();

    }
    public static void AddOnDemandJob()
    {
        OnDemandJob myJob = new OnDemandJob(); //This Constructor needs to be parameterless
        JobDetailImpl jobDetail = new JobDetailImpl("JobOnDemand", "GroupOnDemand", myJob.GetType());
        CronTriggerImpl trigger = new CronTriggerImpl("TriggerOnDemand", "GroupOnDemand", "0 0/5 * * * ?");
        scheduler.ScheduleJob(jobDetail, trigger);
        DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();

    }
    public static void AddZeroJob()
    {
        ZeroJob myJob = new ZeroJob(); //This Constructor needs to be parameterless
        JobDetailImpl jobDetail = new JobDetailImpl("JobZero", "GroupZero", myJob.GetType());
        CronTriggerImpl trigger = new CronTriggerImpl("TriggerZero", "GroupZero", "0 0,10,20,30,40,50 * * * ?");
        scheduler.ScheduleJob(jobDetail, trigger);
        DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();

    }
    public static void AddBusinessJob()
    {
        BusinessJob myJob = new BusinessJob(); //This Constructor needs to be parameterless
        JobDetailImpl jobDetail = new JobDetailImpl("JobBusiness", "GroupBusiness", myJob.GetType());
        CronTriggerImpl trigger = new CronTriggerImpl("TriggerBusiness", "GroupBusiness", "0 0/5 * * * ?");
        //CronTriggerImpl trigger = new CronTriggerImpl("TriggerBusiness", "GroupBusiness", "0 */2 * * * ?");

        scheduler.ScheduleJob(jobDetail, trigger);
        DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();

    }
    public static void AddDailyJob()
    {
        DailyJob myJob = new DailyJob(); //This Constructor needs to be parameterless
        JobDetailImpl jobDetail = new JobDetailImpl("Job4", "Group4", myJob.GetType());
        CronTriggerImpl trigger = new CronTriggerImpl("Trigger4", "Group4", "0 0/5 * * * ?");
        scheduler.ScheduleJob(jobDetail, trigger);
        DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();

    }
    public static void AddMonthlyJob()
    {
        MonthlyJob myJob = new MonthlyJob(); //This Constructor needs to be parameterless
        JobDetailImpl jobDetail = new JobDetailImpl("Job5", "Group5", myJob.GetType());
        CronTriggerImpl trigger = new CronTriggerImpl("Trigger5", "Group5", "0 0/5 * * * ?");
        // CronTriggerImpl trigger = new CronTriggerImpl("Trigger5", "Group5", "0 * * * * ?");
        scheduler.ScheduleJob(jobDetail, trigger);
        DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();
    }
    public static void AddYearlyJob()
    {
        YearlyJob myJob = new YearlyJob(); //This Constructor needs to be parameterless
        JobDetailImpl jobDetail = new JobDetailImpl("Job6", "Group6", myJob.GetType());
        CronTriggerImpl trigger = new CronTriggerImpl("Trigger6", "Group6", "0 0/5 * * * ?");
        // CronTriggerImpl trigger = new CronTriggerImpl("Trigger6", "Group6", "0 * * * * ?");
        scheduler.ScheduleJob(jobDetail, trigger);
        DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();
    }
    public static void AddWeeklyJob()
    {
        WeeklyJob myJob = new WeeklyJob(); //This Constructor needs to be parameterless
        JobDetailImpl jobDetail = new JobDetailImpl("Job7", "Group7", myJob.GetType());
        CronTriggerImpl trigger = new CronTriggerImpl("Trigger7", "Group7", "0 0/5 * * * ?");
        // CronTriggerImpl trigger = new CronTriggerImpl("Trigger7", "Group7",  "0 */2 * * * ?"); //CW: DEBUG
        scheduler.ScheduleJob(jobDetail, trigger);
        DateTimeOffset? nextFireTime = trigger.GetNextFireTimeUtc();
    }


    public static void printDirectories()
    {
        Console.WriteLine("");
        Console.WriteLine("Scripts Directory:         " + ScriptsDirectory);
        Console.WriteLine("Log Directory:             " + LogDirectory);
        Console.WriteLine("Calendar Directory:        " + CalendarDirectory);
        Console.WriteLine("Schedule Table:            " + ScheduleTbl);
        Console.WriteLine("On Demand Schedule Table:  " + OnDemandSchTbl);
        Console.WriteLine("Main Email List:           " + MainEmail);
        Console.WriteLine("Email Switch:              " + (EmailOn ? "ON" : "OFF"));
        Console.WriteLine("");

    }
    public static bool FileExist(String file)
    {
        FileInfo FILE = new FileInfo(file);
        bool exist = FILE.Exists;
        return exist;
    }
    public static bool DirectoryExist(String dir)
    {
        DirectoryInfo DIR = new DirectoryInfo(dir);
        bool exist = DIR.Exists;
        return exist;
    }


}


