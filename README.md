# (**C**)# (**A**)utomated (**T**)ask (**S**)cheduler
## CATS Scheduler
*CATS* is a application made in .Net C# application used to run daily schedules read from a table. Schedules are defined internally in the code. The application uses Quartz .Net Scheduler and Cron schedules to operate. All schedules are concurrent of each other and there is logging for each each schedule. The CATS.config.xml file is required for the application to run and contains 6 parameters need for the application:  
- Scripts Directory  
- Log Directory  
- Calendar Directory  
- Schedule Table    
- On Demand Table  
- Main Email File  
- Email Switch  

 **Features:**  
 - Automatic 25 minute Timeout on each script  
 - Runs Batch, Executables, Perl, and Java.   
 - Emailing on Application Errors and Script Errors  
 - Runs in Parallel(Scripts in Schedule are kicked off at the same time) 
 
 
## CATS.dat
CATS.dat file is the driver of the application. It looks for 3 files, 2 directories and a boolean value. ScriptsDirectory is the location where all the Scripts are kept. *LogDirectory* is the location where you would like your logs for the application to go. *ScheudleTbl* and *ScheduleTestTbl* are the table that the application reads to know what schedules to run at what times. More information on the tables will be below. *MainEmail* is a email distribution  if a error occurs in the program or any scripts ran in the schedule, people are emailed. *EmailOn* boolean turns on and off emailing of the Application.

 
### MainEmail File Format  
 |User1.williams@site.com,User2.williams@site.com|User3.williams@site.com|
 
 Note: From|To|CC|BCC
 
### CATS.dat File Format Example
ScriptsDirectory=C:\Users\63530\Projects\SchedulerC\scripts  
LogDirectory=C:\Users\63530\Projects\SchedulerC\CATS\logs  
CalendarDirectory=C:\Users\63530\Projects\SchedulerC\CATS\Calendars  
ScheduleTbl=C:\Users\63530\Projects\SchedulerC\CATS_SchTable2.dat  
OnDemandSch=C:\Users\63530\Projects\SchedulerC\CATS_SchOnDemand.dat  
MainEmail=C:\Users\63530\Projects\SchedulerC\emailTo\CATS0001.dat  
EmailOn=True  

### CATS Schedule Table Format Example
\#TimeQual|Day|Time|Script|Description  
D|xx|0820|Script1.bat|Comment Example  
M|15|1500|Script2.pl|Comment Example    
0|xx|xxxx|TST_STORM.pl|CATS Storm Testing    
\#5|xx|xxxx|Script4.exe|Comment Example  
\#W|4|1600|Script5.exe|Comment Example   
\#Y|318|1425|Script6.pl|Comment Example   

Lines starting with # are ignored in the program.

### TimeQual
- D - Daily     (D!xx!0820!Script1.bat!Test Table : Description - Execute Everyday at 8:20AM)  
- M - Monthly   (M!15!1500!Script2.pl!Test Table: Description - Execute on the 15th of Every Month at 3:00PM)  
- W - Weekly    (W!4!0020!Script3.exe!Test Table: Description - Execute on Every Wednesday at 12:20AM)  
- Y - Yearly    (Y!319!1424!quicktest5.pl!Test Table : Description - Execute on the 319th day of the year...November 15, 2018) 
- 0 - Zeros     (0!xx!xxxx!quicktest0.pl!Comment Example : Description - Execute on the Zeros (e.g. 5:00,5:10,5:20,5:30,5:40,5:50,6:00,..)
- 5 - Fives     (5!xx!xxxx!quicktest5.pl!Comment Example : Description - Execute on the Fives (e.g. 5:05,5:15,5:25,5:35,5:45,5:55,6:05,..)  
- B - Business  (B!xx!1530!quicktest4.pl!Test Business : Description - Execute during Business days (Monday - Friday)  
- C - Calendar  (C!ACCOUNTING!2255!quicktest3.pl!Test Business : Description - Execute schedule based on a Calendar (See Below for more information)  

For Weekly Scheudles: Sunday - 1, Monday - 2 Tuesday - 3,Wednesday - 4, Thursday - 5,Friday - 6, Saturday - 7.  
For the purpose of the README.md file, I have changed the Pipe in the Example Detail to the Exclamation point. Bitbucket will not display the PIPE correctly.  

### Calendar Tables Details

#### Calendar Table Format  
Year|Day Of Year|Description (Do not include this in your table file)  
2019|35|JAN-dd  
2019|63|FEB-dd  
2019|140|MAR-dd  
2019|126|APR-dd  
2019|154|MAY-dd  
2019|182|JUN-dd  
2019|217|JUL-dd  
2019|245|AUG-dd  
2019|273|SEP-dd  
2019|308|OCT-dd  
2019|336|NOV-dd  
2019|365|DEC-dd  

 - For Schedules on a Calendar, the second position indicates the subname of the Calendar. In the above example ACCOUNTING is the sub-name. 
   The sub-name MUST be prefixed with "SON_CATSCal_" and end in ".dat" (e.g. SON_CATSCal_ACCOUNTING.dat).   
 - All Calendar files must be placed in the Calendar Directory Folder.
 
### On Demand Table
- The On Demand table is a text file that the application reads to run Scripts on the next run when the user inputs a script. After running, the file is emptied.  

####Notes:
- Daily,Monthly,Weekly,Business,Calendar, and Yearly Schedule kick-offs are ran every five minutes. Validation to run the schedule are read from the ScheduleTable  
- Use Full script names in the schedule. (e.g. JavaProgram.class, Test1.bat).  
- Other programs, like the ones already in the application, can be called through Command-line batch.For example, if you would like to run Python Code, create a batch Script and call it.  
 
### Helper Sites
* [Quartz .Net Scheduler] - The Spring Framework provides abstractions for asynchronous execution and scheduling of tasks!
* [Cron Examples] -The quick and simple editor for cron schedule expressions by Cronitor!


[Quartz .Net Scheduler]: <https://www.quartz-scheduler.net/>
[Cron Examples]: <https://crontab.guru/examples.html>
