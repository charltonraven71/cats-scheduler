﻿using Quartz;
using System;
using System.IO;
using System.Threading.Tasks;

class UpdateTableJob : IJob
{

    public static void UpdateTable()
    {
        JobScheduler.schs.Clear();
        StreamReader sr = new StreamReader(JobScheduler.ScheduleTbl);
        String line;
        while ((line = sr.ReadLine()) != null)
        {
            if (!line.StartsWith("#")&& !line.Trim().Equals(""))
            {
                String[] schSplit = line.Split('|');
                String timeQual = schSplit[0];
                String day = schSplit[1];
                String time = schSplit[2];
                String script = schSplit[3];
                String description = schSplit[4];
                JobScheduler.schs.Add(new Schedule(timeQual, day, time, script, description));
            }

        }
        sr.Close();
        sr.Dispose();
    }
    public async Task Execute(IJobExecutionContext context)
    {
        UpdateTable();


    }
}



