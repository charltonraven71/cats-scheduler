﻿using Quartz;
using System;
using System.Threading.Tasks;


class MonthlyJob : IJob
{
    public async Task Execute(IJobExecutionContext context)
    {
        String Qual = "M";
        RunUtil run = new RunUtil(Qual, DateTime.Now);
        run.GetAndExecute();
    }
}
