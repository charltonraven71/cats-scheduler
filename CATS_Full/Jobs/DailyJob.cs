﻿using Quartz;
using System;
using System.Threading.Tasks;


class DailyJob : IJob
{
    public async Task Execute(IJobExecutionContext context)
    {
        String Qual = "D";
        RunUtil run = new RunUtil(Qual, DateTime.Now);
        run.GetAndExecute();
    }
}
