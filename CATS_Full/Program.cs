﻿using System;
using System.IO;
using System.Globalization;


namespace CATS
{
    class Program
    {
        static void Main(string[] args)
        {

            if (args.Length == 0)
            {
                JobScheduler scheduler = new JobScheduler();
                scheduler.Start();
                Console.ReadKey();
                scheduler.Stop();
            }
            else
            {
                if (args.Length > 0)
                {
                    String command = args[0].ToUpper();
                    int neededArguments = 0;
                    int numOfArgs = args.Length;
                    switch (command.ToLower())
                    {
                        case "/?":
                            help();
                            return;
                        case "/add":
                            AddToSchedule();
                            return;
                        default:
                            Console.WriteLine("Command Not Found");
                            help();
                            return;
                    }


                }
            }
        }

        private static void help()
        {
            throw new NotImplementedException();
        }

        private static void AddToSchedule()
        {
            Console.WriteLine("Schedule Type (D - Daily, M - Monthly, W - Weekly, Y - Yearly): ");
            String ScheduleType = Console.ReadLine();

            Console.WriteLine("Name of Script");
            String ScriptName = Console.ReadLine();

            Console.WriteLine("Enter a Description");
            String Description = Console.ReadLine();


            if (ScheduleType.ToLower().Equals("d"))
            {
                CreateDailySchedule(ScriptName, Description);
            }
            else if (ScheduleType.ToLower().Equals("w"))
            {
                CreateWeeklySchedule(ScriptName, Description);
            }

            else if (ScheduleType.ToLower().Equals("m"))
            {
                CreateMonthlySchedule(ScriptName, Description);
            }

            else if (ScheduleType.ToLower().Equals("y"))
            {
                CreateYearlySchedule(ScriptName, Description);
            }
            
        }

        public static void CreateDailySchedule(String ScriptName,String Description)
        {
            DateTime date = DateTime.Now;
            String currentDateSTR = date.ToString("MM/dd/yyyy HH:mm");
            
            Console.WriteLine("What Time(s), Separate by comma(,): (e.g. '1400,1520')");
            String timesCriteria = Console.ReadLine().ToLower();

            String scheduleTbl = getScheduleTable();
            //Add Date and Time to Description
            Description = Description + " Added On " + currentDateSTR;
            StreamWriter sw = new StreamWriter(scheduleTbl,true);

            if (timesCriteria.Split(',').Length == 1)
            {
                
                sw.WriteLine("D|xx|" + timesCriteria.Trim() + "|" + ScriptName + "|" + Description);

            }
            else if(timesCriteria.Split(',').Length > 1)
            {
                String[] times = timesCriteria.Split(',');
                for(int i = 0; i < times.Length; i++)
                {
                    sw.WriteLine("D|xx|" + times[i].Trim() + "|" + ScriptName + "|" + Description);

                }
            }
            sw.Close();

            Console.WriteLine("Times Added to table");
        }
        public static void CreateWeeklySchedule(String ScriptName, String Description)
        {
            Console.WriteLine("What day of the week? (1 - SUN, 2 - MON, 3 - TUES, 4 - WEN ,5 - THUR, 6 - FRI , 7 - SAT)");
            String DayOfTheWeek = Console.ReadLine();
            DateTime date = DateTime.Now;
            String currentDateSTR = date.ToString("MM/dd/yyyy HH:mm");

            Console.WriteLine("What Time(s), Separate by comma(,): (e.g. '1400,1520')");
            String timesCriteria = Console.ReadLine().ToLower();

            String scheduleTbl = getScheduleTable();
            //Add Date and Time to Description
            Description = Description + " Added On " + currentDateSTR;
            StreamWriter sw = new StreamWriter(scheduleTbl, true);

            if (timesCriteria.Split(',').Length == 1)
            {

                sw.WriteLine("W|"+ DayOfTheWeek.Trim()+"|" + timesCriteria.Trim() + "|" + ScriptName + "|" + Description);

            }
            else if (timesCriteria.Split(',').Length > 1)
            {
                String[] times = timesCriteria.Split(',');
                for (int i = 0; i < times.Length; i++)
                {
                    sw.WriteLine("W|" + DayOfTheWeek.Trim() + "|" + times[i].Trim() + "|" + ScriptName + "|" + Description);
                }
            }
            sw.Close();

            Console.WriteLine("Times Added to table");


        }
        public static void CreateMonthlySchedule(String ScriptName, String Description)
        {
            Console.WriteLine("What day of the Month? (1-31)");
            String DayOfTheMonth = Console.ReadLine();
            DateTime date = DateTime.Now;
            String currentDateSTR = date.ToString("MM/dd/yyyy HH:mm");

            Console.WriteLine("What Time(s), Separate by comma(,): (e.g. '1400,1520')");
            String timesCriteria = Console.ReadLine().ToLower();

            String scheduleTbl = getScheduleTable();
            //Add Date and Time to Description
            Description = Description + " Added On " + currentDateSTR;
            StreamWriter sw = new StreamWriter(scheduleTbl, true);

            if (timesCriteria.Split(',').Length == 1)
            {

                sw.WriteLine("W|" + DayOfTheMonth.Trim() + "|" + timesCriteria.Trim() + "|" + ScriptName + "|" + Description);

            }
            else if (timesCriteria.Split(',').Length > 1)
            {
                String[] times = timesCriteria.Split(',');
                for (int i = 0; i < times.Length; i++)
                {
                    sw.WriteLine("W|" + DayOfTheMonth.Trim() + "|" + times[i].Trim() + "|" + ScriptName + "|" + Description);
                }
            }
            sw.Close();

            Console.WriteLine("Times Added to table");
        }
        public static void CreateYearlySchedule(String ScriptName, String Description)
        {

            Console.WriteLine("What Date? (e.g. 4/5/2013)");
            String ReadDate = Console.ReadLine().Trim();
            DateTime parseDate;
            if(DateTime.TryParseExact(ReadDate, "M/d/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None,out parseDate))
            {

                DateTime date = DateTime.Now;
                String currentDateSTR = date.ToString("MM/dd/yyyy HH:mm");

                Console.WriteLine("What Time(s), Separate by comma(,): (e.g. '1400,1520')");
                String timesCriteria = Console.ReadLine().ToLower();

                String scheduleTbl = getScheduleTable();
                //Add Date and Time to Description
                Description = Description + " Added On " + currentDateSTR;
                StreamWriter sw = new StreamWriter(scheduleTbl, true);

                if (timesCriteria.Split(',').Length == 1)
                {

                    sw.WriteLine("Y|" + parseDate.DayOfYear + "|" + timesCriteria.Trim() + "|" + ScriptName + "|" + Description);

                }
                else if (timesCriteria.Split(',').Length > 1)
                {
                    String[] times = timesCriteria.Split(',');
                    for (int i = 0; i < times.Length; i++)
                    {
                        sw.WriteLine("Y|" + parseDate.DayOfYear + "|" + times[i].Trim() + "|" + ScriptName + "|" + Description);
                    }
                }
                sw.Close();

                Console.WriteLine("Times Added to table");
            }
            else
            {
                Console.WriteLine("Invalid Date Format");
            }

        }
        public static String getScheduleTable()
        {
            String architecture = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE");

            String configFile = "";

            if (architecture.Equals("x86"))
            {
                configFile = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFiles) + @"\Sonoco EDI\CATS\CATS.dat";
            }
            else if (architecture.Equals("x64"))
            {
                configFile = Environment.GetFolderPath(Environment.SpecialFolder.ProgramFilesX86) + @"\Sonoco EDI\CATS\CATS.dat";
            }


            if (!FileExist(configFile))
            {
                FileInfo createCATSFile = new FileInfo(configFile);
                createCATSFile.Create();
                Console.WriteLine("CATS.dat file did not exist. A CATS.dat file has been created  in the " + createCATSFile.Directory + " folder. Application has exited. Please enter necessary information into the CATS.dat file and rerun the application.");
                Console.Read();
                Environment.Exit(0);
            }
            StreamReader sr = null;
            if (FileExist(configFile))
            {
                //Console.WriteLine("Getting Configurations for Files and Directories...");
                sr = new StreamReader(configFile);
            }
            else
            {
                Console.WriteLine("CATS.dat file does not exist in Program Files Directory.");
                Environment.Exit(0);
            }
            String ScheduleTbl = "";
            String line;
            while ((line = sr.ReadLine()) != null)
            {

                if (line.StartsWith("ScheduleTbl"))
                {
                    ScheduleTbl = line.Split('=')[1].Replace(Environment.NewLine, "");
                }

            }
            sr.Close();

            return ScheduleTbl;
        }
        public static bool FileExist(String file)
        {
            FileInfo FILE = new FileInfo(file);
            bool exist = FILE.Exists;
            return exist;
        }


    }
}
