﻿using Quartz;
using System;
using System.Threading.Tasks;


class CalendarJob : IJob
{
    public async Task Execute(IJobExecutionContext context)
    {
        String Qual = "C";
        RunUtil run = new RunUtil(Qual, DateTime.Now);
        run.GetAndExecute();
    }
}
