﻿using Quartz;
using System;
using System.Threading.Tasks;


class WeeklyJob : IJob
{
    public async Task Execute(IJobExecutionContext context)
    {
        String Qual = "W";
        RunUtil run = new RunUtil(Qual, DateTime.Now);
        run.GetAndExecute();
    }
}
