﻿using Quartz;
using System;
using System.Threading.Tasks;


class FiveJob : IJob
{

    public async Task Execute(IJobExecutionContext context)
    {
        String Qual = "5";
        RunUtil run = new RunUtil(Qual, DateTime.Now);
        run.GetAndExecute();
    }
}

