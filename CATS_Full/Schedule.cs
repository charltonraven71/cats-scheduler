﻿using System;


public class Schedule
{
    private String timeQual;
    private String day;
    private String time;
    private String script;
    private String description;
    private String runDate;
    private String runTime;
    private String calendarName = "";

    

    public Schedule(string timeQual, string multiValue, string time, string script, string description)
    {
        this.TimeQual = timeQual;

        if (timeQual.Equals("C"))
        {
            CalendarName = multiValue;
        
        }
        else
        {
            Day = multiValue;
        }

        this.Time = time;
        this.Script = script;
        this.Description = description;

    }

    public string TimeQual { get => timeQual; set => timeQual = value; }
    public string Day { get => day; set => day = value; }
    public string Time { get => time; set => time = value; }
    public string Script { get => script; set => script = value; }
    public string Description { get => description; set => description = value; }
    public string CalendarName { get => calendarName; set => calendarName = value; }
}
